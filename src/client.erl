-module(client).
-export([compute_factorial/2]).

compute_factorial(Pid, N) ->
    Ref = make_ref(),
    Pid ! {factorial, self(), Ref, N},
    receive
        {result, Ref, Result} ->
            Result
    end.
