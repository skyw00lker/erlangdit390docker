-module(proxy). 
-export([]).



proxy(Server, C, MaxReq) ->
    receive
        {request, From, Ref, Request} when C =< MaxReq ->
            From ! {request_processing, Ref},
            Server ! {request, Ref, Request},
            proxy(Server, C+1, MaxReq);
       {request, From, Ref, Request} when C > MaxReq ->
            From ! {request_denied, Ref},
            proxy(Server, C, MaxReq),
       ready_to_server -> proxy(Server, C-1, MaxReq)
    end.
