-module(resources).

-export([start/1, request/1, release/1]).

loop(Resources) ->
    Available = length(Resources),

    receive
        {req, From, Ref, Number} when Number =< Available ->
            From ! {res, Ref, lists:sublist(Resources, Number)},
            loop(lists:sublist(Resources, Number+1, Available));
        {ret, List} -> loop(lists:append(Resources,List))
    end.


start(Init) ->
    Pid = spawn(fun() -> loop(Init) end),
    register(rserver, Pid).


request(N) ->
    Ref = make_ref(),
    rserver ! {req, self(), Ref, N},
    receive
        {res, Ref, List} -> List
    end.


release(List) ->
    rserver ! {ret, List},
    ok.




