-module(counter).
-export([start/0]).


start() ->
    main:start(server, 0, fun counter/2).

counter(Count, add_one) ->
    {Count + 1, Count + 1}.
