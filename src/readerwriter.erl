-module(readerwriter).
-export([example/1]).


loop() -> 
    receive 
        {start_read, From, Ref} ->
            From ! {ok_to_read, Ref},
            loop_read(1),
            loop();
        {start_write, From, Ref} ->
            From ! {ok_to_write, Ref},
            receive
                end_write -> loop()
            end
    end.




loop_read(0) -> ok;

loop_read(Rs) ->
    receive
        {start_read, From, Ref} ->
            From ! {ok_to_read, Ref},
            loop_read(Rs + 1);
        end_read -> loop_read(Rs-1);

        {start_write, From, Ref} ->
            [ receive end_read -> ok end || _ <- lists:seq(1,Rs) ],
            From ! {ok_to_write, Ref},
            receive 
                end_write -> ok
            end
    end.

start() ->
    Pid = spawn(fun loop/0),
    register(server, Pid).


begin_read() ->
    Ref = make_ref(),
    server ! {start_read, self(), Ref}, 
    receive
        {ok_to_read, Ref} -> ok
    end.

begin_write() ->
    Ref = make_ref(),
    server ! {start_write, self(), Ref},
    receive
        {ok_to_write, Ref} -> ok
    end.

end_read() ->
    server ! end_read,
    ok.

end_write() ->
    server ! end_write,
    ok.



pread(I) ->
    io:format("preparing for reading ~w~n", [I]),
    begin_read(),
    timer:sleep(random:uniform(800)),
    end_read(),
    io:format("Finished reading ~w~n", [I]).

pwrite(I) ->
    io:format("Preparing for writing~w~n", [I]),
    begin_write(),
    timer:sleep(random:uniform(2000)),
    end_write(),
    io:format("Finished writing~w~n", [I]).


example(N) ->
    start(),
    [spawn(fun () -> pread(X) end) || X <- lists:seq(1,N)],
    %spawn(fun () -> pread(1) end),
    spawn(fun () -> pwrite(1) end),
    [spawn(fun () -> pread(X) end) || X <- lists:seq(N+1,2*N)],
    ok.
