-module(mvar).
-export([example/1]).

e_initMVar() ->
    Pid = spawn(fun() -> loop(0, empty) end),
    register(mvar, Pid).

e_putMVar(X) ->
    Ref = make_ref(),
    mvar ! {put_mvar, self(), Ref, X},
    receive 
        {ok, Ref} -> ok
    end.

e_takeMVar() ->
    Ref = make_ref(),
    mvar ! {take_mvar, self(), Ref},
    receive
        {ok, Ref, Value} -> Value
    end.


loop(State, Status) ->
    receive
        {put_mvar, From, Ref, Value} when Status =:= empty ->
            io:format("Putting mvar ~w~n", [Value]),
            From !  {ok, Ref},
            loop(Value, full);
        {take_mvar, From, Ref} when Status =:= full ->
            io:format("Taking mvar ~w~n", [State]),
            From ! {ok, Ref, State},
            loop(State, empty)
    end.



ptake(I) ->
    Res = e_takeMVar().

pput(I) ->
    timer:sleep(random:uniform(500)),
    e_putMVar(I).

start() ->
    e_initMVar().


example(N) ->
    start(),
    [spawn(fun() -> ptake(X) end) || X <- lists:seq(1, N)],
    [spawn(fun() -> pput(X) end) || X <- lists:seq(1, N)].
